---
title: Định nghĩa, lợi ích của AWS Global Infrastructure
date: 2021-10-12
categories:
 - aws
tags:
 - aws
---
## Định nghĩa
AWS Global Infrastructure là một Cloud Platform, một cơ sở hạ tầng đám mây. Nó đứng số 1 về các tiêu chí sau
- Tính an toàn (Đối với những nguy hiểm, nguy hại vẫn có sức chịu đựng trong ngưỡng an toàn)
- Tính trải rộng (Khắp thế giới, không chỉ tập trung ở một vùng lãnh thổ nhất định nào)
- Độ tin tưởng (Có khả năng hoạt động ổn định ở những điều kiện và thời gian quy định)

AWS cung cấp hơn 200 dịch vụ từ khắp các trung tâm dữ liệu trên toàn thế giới.
![aws_global_infrastructure](/imgs/aws_infrastructure.png)

## Lợi ích khi sử dụng AWS Global Infrastructure
Điểm mạnh khi sử dụng các dịch vụ của Cloud Platform
- Triển khai hệ thống, ứng dụng ở bất kì đâu trên thế giới chỉ với một cú click chuột
- Giảm độ trễ của ứng dụng, hệ thống xuống mức thấp nhất (vài mili giây)
- Cung cấp hàng chục nghìn đối tác quốc tế tạo lên hệ sinh thái năng động, rộng lớn

Các lợi ích khi sử dụng AWS Global Infrastructure
- Tính bảo mật: Tất cả dữ liệu đều được mã hóa tự động và được kiểm soát 24/7
- Tính khả dụng: Cung cấp nhiều vùng sẵn sàng và được tách lập hoàn toàn trong cơ sở hạ tầng, giúp cho mức độ sẵn sàng cao hơn khi gặp sự cố (động đất, sóng thần?)
- Hiệu năng cao: Nhanh chóng khởi chạy tài nguyên, duy trì độ trễ thấp cho ứng dựng hệ thống
- Phạm vi toàn cầu: Có cơ sở hạ tầng đám mây lớn nhất
- Tính mở rộng: AWS cho phép người dùng có thể mở rộng ứng dụng, hệ thống một cách vô hạn trên cơ cở hạ tầng đám mây
- Tính linh hoạt: Cung cấp nhiều lựa chọn để người dùng có thể tùy chọn. Người dùng có thể chọn cho chạy ứng dụng ở bất kỳ khu vực nào, phù hợp với độ trễ cho phép, phù hợp với khối lượng dữ liệu cần xử lý

## Tham khảo
[AWS Global Infrastructure](https://aws.amazon.com/about-aws/global-infrastructure/?nc1=h_ls)
