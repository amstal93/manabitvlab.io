---
title: Chạy thử nghiệm model tensorflow lite cho bài toán nhận diện và tracking
date: 2021-10-21
categories:
 - ai
tags:
 - tensorflow lite
 - ai
---
## Môi trường python
Mặc định chúng ta đã có python và pip. Nếu chưa có thể cài đặt như sau
```bash
$ sudo apt-get install python3-pip
$ sudo apt-get update
$ sudo apt-get install python3-venv
```
### Tạo một môi trường python cho tensorflow
```bash
$ python3 -m venv tf
$ source tf/bin/activate
```
Sau đó cài đặt tensorflow và opencv nếu cần
```bash
$ pip install tensorflow
$ pip install opencv-contrib-python
```

## Cài đặt tensorflow lite runtime
Tùy vào version python để chọn cho phù hợp, với python 3.6.9 là mặc định của Ubuntu 18.04 thì có thể dùng lệnh sau
```bash
$ pip3 install https://dl.google.com/coral/python/tflite_runtime-2.1.0.post1-cp36-cp36m-linux_x86_64.whl
```

## Cài đặt thư viện cho Coral
Coral USB accelator là một USB giúp cho model có thể sử dụng TPU để tăng tốc độ tính toán của model. Cài đặt như sau
```bash
$ sudo apt-get install curl
$ echo "deb https://packages.cloud.google.com/apt coral-edgetpu-stable main" | sudo tee /etc/apt/sources.list.d/coral-edgetpu.list
$curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
$ sudo apt-get update
```

Tiếp theo là cài đặt thư viện để sử dụng tpu, có 2 options, và chỉ có thể cài đặt 1 trong 2. Cài đè lên thì thứ được cài đặt trước đó sẽ mất
```bash
# standard:
sudo apt-get install libedgetpu1-std
# max:
sudo apt-get install libedgetpu1-max
```
	
Tất nhiên khi chạy model dưới dạng tflite thì độ chính xác sẽ bị giảm đi, giảm bao nhiêu còn phụ thuộc vào model.
Để xem được những ví dụ cho từng nhiệm vụ (classification, detection, segmentation etc), có thể tham khảo tại [đây](https://www.tensorflow.org/lite/examples).


## Chạy thử ssd mobilenet bản tflite
Đầu tiên chuẩn bị thư mục chứa model và nhãn coco, model này đã được training trên tập coco dataset và chuyển về dưới dạng tflite.
Khi chọn model cần lưu ý, nếu chọn model được mô hình hóa riêng cho TPU edge device thì bắt buộc cần phải có TPU edge device.
- Tham khảo [đây](https://coral.ai/models/object-detection/) để chọn model cho đúng. Trong bài này không dung TPU nên chỉ cần chọn model thường chạy trên CPU

Trong hướng dẫn này chọn SSD MobileNet V2 có tf version 1 vì có MAP cao hơn.
Để kiếm thử thì có thể dùng repository dưới đây, reopos này có thể:
- Detect người dùng tflite ssd_mobilenetv2
- Tracking người dùng CentertroidTracker
- Đếm người qua lại trái và phải
- Vì chạy tflite nên đạt tốc độ realtime trên CPU và cả trên raspi có TPU (CoralUSB accelator)
```bash
$ git clone  https://github.com/sfbsoft/person_counter.git
$ cd person_counter
$ python3 tflite_demo.py
```

## Kết quả
![tflite_detection_tracking](/imgs/tflite_people_detection.png)


## Refer
- [Source code](https://github.com/sfbsoft/person_counter.git)
