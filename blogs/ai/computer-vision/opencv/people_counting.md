---
title: Đếm số lượng người qua lại chạy trên Raspi
date: 2021-10-11
categories:
 - ai
tags:
 - ai
 - computer vision
---

Trong project này
- Mục đích là để đếm số lượng người trong cửa hàng, tòa nhà một cách realtime.
- Gửi thông báo tới cho nhân viên quản lý
- Tính năng chạy với threading để cho hiệu năng tốt hơn


## Lý thuyết
### SSD detector
- Trong project này sẽ dùng SSD detector (Single Shot Detector) với kiến trúc mạng MobileNet. SSD cho 
biết có những object nào trong bức ảnh.
- So sánh với R-CNN thì SSD khá là nhanh
- Kiến trúc mạng MobileNet là một kiến trúc DNN được thiết kế dành riêng cho những thiết bị phần cứng có hiệu năng nhỏ 
như mobile, ipcamera, hay là máy in etc...

### Centroid tracker
- Centroid tracker là một trong những tracker chạy ổn định
- Centroid chính là điểm trung tâm của bouding boxes
- Nó cũng chính là centroid của object mà SSD trả về
- Nó được gán cho một ID duy nhất và được tracking qua các frame liên tục

## Chuẩn bị
### Pull source code
```bash
$ git clone https://github.com/saimj7/People-Counting-in-Real-Time.git app
```

### Install dependencies library
```bash
$ cd app
$ sudo apt-get install cmake
$ pip install -r requirements.txt 
```

### Start x-server
Phần setting này để chạy được ứng dụng GUI từ docker container trên WSL2 của Windows. Nếu không trong docker container trên WSL2 thì có thể bỏ qua phần này
- Cài đặt docker cho windows
- Cài đặt docker trên WSL2
- Cài đặt x-server trên windows
- Khi start thì check hết vào các options ở màn hình setting cuối
- Sau khi start x-server xong thì cần tạo một biến môi trường cho DISPLAY
```bash
$ export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2}'):0
```

### Chạy chương trình
```bash
$ cd app
$ python3 run.py --prototxt mobilenet_ssd/MobileNetSSD_deploy.prototxt --model mobilenet_ssd/MobileNetSSD_deploy.caffemodel --input videos/example_02.mp4
```

## Reference link
- [Still not support run fastmot on CPU](https://github.com/GeekAlexis/FastMOT/issues/48)
- [Run on CPU](https://github.com/adipandas/multi-object-tracker)
- [FastMOT](https://github.com/GeekAlexis/FastMOT)
- [Multiple Object Tracking](https://tech-blog.optim.co.jp/entry/2021/07/07/100000)
- [Deepsort, Kalman Filter, Centroid Tracking](https://towardsdatascience.com/people-tracking-using-deep-learning-5c90d43774be)
- [Mobilnetssd + yolov3 comparsion](https://cristianpb.github.io/blog/ssd-yolo)
- [Kalman filter + mobilenetssdv2](https://github.com/JardinRyu/Jetson_Nano_People_Counting)
- [Yolov3 + deepsort](https://github.com/yehengchen/Object-Detection-and-Tracking/tree/master/OneStage/yolo/deep_sort_yolov3)
- [Detection + tracking](https://github.com/yehengchen/Object-Detection-and-Tracking)
- [Tensorflow 2 Object Counting](https://github.com/TannerGilbert/Tensorflow-2-Object-Counting)
- [Single-Multiple-Custom-Object-Detection-and-Tracking](https://github.com/emasterclassacademy/Single-Multiple-Custom-Object-Detection-and-Tracking)
- [Opencv only](https://github.com/sarful/People-counter-opencv-python3)
- [Opencv only](https://github.com/Gupu25/PeopleCounter)
- [Raspi + tpu + deepsort](https://github.com/nathanrooy/rpi-urban-mobility-tracker)
- [Yolov3 mobilnetssd yolov5s comparison](https://www.irjet.net/archives/V8/i7/IRJET-V8I7193.pdf)
